#include "stdafx.h"
#include "Renderer.h"
#include "LoadPng.h"
#include <Windows.h>
#include <cstdlib>
#include <cassert>
#include <cmath>
#include <iostream>
#include <random>
using namespace std;

Renderer::Renderer(int windowSizeX, int windowSizeY)
{
	Initialize(windowSizeX, windowSizeY);
}


Renderer::~Renderer()
{
}

void Renderer::Initialize(int windowSizeX, int windowSizeY)
{
	//Set window size
	m_WindowSizeX = windowSizeX;
	m_WindowSizeY = windowSizeY;

	//Load shaders
	m_SolidRectShader = CompileShaders("./Shaders/SolidRect.vs", "./Shaders/SolidRect.fs");
	
	//Create VBOs
	CreateVertexBufferObjects();
}

void Renderer::CreateVertexBufferObjects()
{
	float rect[]
		=
	{
		-0.5, -0.5, 0.f, -0.5, 0.5, 0.f, 0.5, 0.5, 0.f, //Triangle1
		-0.5, -0.5, 0.f,  0.5, 0.5, 0.f, 0.5, -0.5, 0.f, //Triangle2
	};


	glGenBuffers(1, &m_VBORect); //버텍스 버퍼오브젝트(vbo)의 id를 만들어준다. GPU메모리 할당이 이루어지지않음. 인덱스 역할을해준다.
	glBindBuffer(GL_ARRAY_BUFFER, m_VBORect);
	glBufferData(GL_ARRAY_BUFFER, sizeof(rect), rect, GL_STATIC_DRAW);

	//GPU의 메모리로 전환해야함. CPU의 메인메모리 -> GPU의 TEXTURE 메모리
	float vertices[]
		=
	{
		0.f,0.f,0.f, // v0
		1.f, 0.f, 0.f, // v1
		1.f, 1.f, 0.f // v2

		// 이름
		//-0.75f,0.5f,0.0f,  -0.75f,0.0f,0.0f,   -0.5f,0.0f,0.0f,  // L
		//0.25f,0.5f,0.0f,   0.0f,0.5f,0.0f,   -0.25f,0.25f,0.0f,   0.0f,0.0f,0.0f,    0.25f,0.0f,0.0f,   //C
		//0.5f,0.25f,0.0f,   0.5f,0.0f,0.0f,   0.625f,0.0f,0.0f,    0.625f,0.5f,0.0f,    0.5f,0.5f,0.0f,   0.75f,0.5f,0.0f  //J
	}; 
	
	glGenBuffers(1, &m_VBOTest); // ID만 만든것.
	glBindBuffer(GL_ARRAY_BUFFER, m_VBOTest); //어떤용도로 사용될거냐 ? 라는것을 바인딩 해주는것.
	//우리가 만든 버퍼는 ARRAY 형태로 ACCESS 할것이다. //여기서는 용도를 부여함. 
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW); //실제 Bind된 VBO에 데이터를 할당하는 것. 

	float color[]
		=
	{
		1, 0, 0, 1, // V0
		0, 1, 0, 1, // V1
		0, 0, 1, 1 // v2
	};

	glGenBuffers(1, &m_VBOTestColor); // ID만 만든것.
	glBindBuffer(GL_ARRAY_BUFFER, m_VBOTestColor); //어떤용도로 사용될거냐 ? 라는것을 바인딩 해주는것.
	//우리가 만든 버퍼는 ARRAY 형태로 ACCESS 할것이다. //여기서는 용도를 부여함. 
	glBufferData(GL_ARRAY_BUFFER, sizeof(color), color, GL_STATIC_DRAW); //실제 Bind된 VBO에 데이터를 할당하는 것
	
	float particleSize = 0.01f;

	float singleParticleVertices[]
		=
	{
		-1.f * particleSize ,-1.f * particleSize  ,0.f * particleSize, // v0
		1.f * particleSize , 1.f * particleSize  , 0.f * particleSize, // v1
		-1.f * particleSize  , 1.f * particleSize , 0.f * particleSize, // v2
		-1.f * particleSize  ,-1.f * particleSize  ,0.f * particleSize, // v3
		1.f * particleSize ,  -1.f * particleSize  , 0.f * particleSize, // v4
		1.f * particleSize  , 1.f * particleSize  , 0.f * particleSize // v5

	};

	glGenBuffers(1, &m_VBOSingleParticle); // ID만 만든것.
	glBindBuffer(GL_ARRAY_BUFFER, m_VBOSingleParticle); //어떤용도로 사용될거냐 ? 라는것을 바인딩 해주는것.
	//우리가 만든 버퍼는 ARRAY 형태로 ACCESS 할것이다. //여기서는 용도를 부여함. 
	glBufferData(GL_ARRAY_BUFFER, sizeof(singleParticleVertices), singleParticleVertices, GL_STATIC_DRAW); //실제 Bind된 VBO에 데이터를 할당하는 것.  

	//Create Many PArticles;
	CreateParticle(10000);
}

void Renderer::CreateParticle(int count)
{
	float* particleVertices = new float[count * (11+4) * 3 * 2]; // 2개의 삼각형, 삼각형마다 3개의 버텍스, 버텍스는 xyz 3개의 float
	int floatCount = count * (11+4) * 3 * 2;
	int vertexCount = count * 3 * 2;

	int index = 0;
	float particleSize = 0.01f;
	for (int i = 0; i < count; ++i)
	{
		// quad -> 1particle -> vertex should have same velocity
		float randomValueX = 0.f;
		float randomValueY = 0.f;
		float randomValueZ = 0.f;

		float randomValueVX = 0.f;
		float randomValueVY = 0.f;
		float randomValueVZ = 0.f;

		float randomStartTime = 0.f;
		float randomLifeTime = 0.f;

		float randomPeriod = 0.f;
		float randomAmp = 0.f;

		float randomValue = 0.f;
		float randomColorR = 0.f;
		float randomColorG = 0.f;
		float randomColorB = 0.f;
		float randomColorA = 0.f;

		randomValueX = 0.f;//(rand() / (float)RAND_MAX - 0.5f) * 2.f; // -1 ~ 1 사이
		randomValueY = 0.f;//(rand() / (float)RAND_MAX - 0.5f) * 2.f;
		randomValueZ = 0.f; // 

		randomValueVX = 0.f;//(rand() / (float)RAND_MAX - 0.5f) * 0.5f; 
		randomValueVY = 0.f;// (rand() / (float)RAND_MAX - 0.5f) * 0.5f;
		randomValueVZ = 0.f;

		randomStartTime = (rand() / (float)RAND_MAX - 0.5f) * 6.f; //0~6초 사이
		randomLifeTime = (rand() / (float)RAND_MAX - 0.5f) * 3.f;

		randomPeriod = (rand() / (float)RAND_MAX - 0.5f) * 3.f; 
		randomAmp = (rand() / (float)RAND_MAX - 0.5f) * 0.0f;
		
		randomValue = (rand() / (float)RAND_MAX);
		randomColorR = (rand() / (float)RAND_MAX);
		randomColorG = (rand() / (float)RAND_MAX);
		randomColorB = (rand() / (float)RAND_MAX);
		randomColorA = 1.0f;

		// v1
		particleVertices[index] = -particleSize / 2.f + randomValueX;
		index++;
		particleVertices[index] = -particleSize / 2.f + randomValueY;
		index++;
		particleVertices[index] = 0.f;
		index++;
		particleVertices[index] = randomValueVX;
		index++;
		particleVertices[index] = randomValueVY;
		index++;
		particleVertices[index] = randomValueVZ;
		index++;
		particleVertices[index] = randomStartTime;
		index++;
		particleVertices[index] = randomLifeTime;
		index++;
		particleVertices[index] = randomPeriod;
		index++;
		particleVertices[index] = randomAmp;
		index++;
		particleVertices[index] = randomValue;
		index++;
		particleVertices[index] = randomColorR;
		index++;
		particleVertices[index] = randomColorG;
		index++;
		particleVertices[index] = randomColorB;
		index++;
		particleVertices[index] = randomColorA;
		index++;

		// v2
		particleVertices[index] = particleSize / 2.f + randomValueX;
		index++;
		particleVertices[index] = -particleSize / 2.f + randomValueY;
		index++;
		particleVertices[index] = 0.f;
		index++;
		particleVertices[index] = randomValueVX;
		index++;
		particleVertices[index] = randomValueVY;
		index++;
		particleVertices[index] = randomValueVZ;
		index++;
		particleVertices[index] = randomStartTime;
		index++;
		particleVertices[index] = randomLifeTime;
		index++;
		particleVertices[index] = randomPeriod;
		index++;
		particleVertices[index] = randomAmp;
		index++;
		particleVertices[index] = randomValue;
		index++;
		particleVertices[index] = randomColorR;
		index++;
		particleVertices[index] = randomColorG;
		index++;
		particleVertices[index] = randomColorB;
		index++;
		particleVertices[index] = randomColorA;
		index++;

		// v3
		particleVertices[index] = particleSize / 2.f + randomValueX;
		index++;
		particleVertices[index] = particleSize / 2.f + randomValueY;
		index++;
		particleVertices[index] = 0.f;
		index++;
		particleVertices[index] = randomValueVX;
		index++;
		particleVertices[index] = randomValueVY;
		index++;
		particleVertices[index] = randomValueVZ;
		index++;
		particleVertices[index] = randomStartTime;
		index++;
		particleVertices[index] = randomLifeTime;
		index++;
		particleVertices[index] = randomPeriod;
		index++;
		particleVertices[index] = randomAmp;
		index++;
		particleVertices[index] = randomValue;
		index++;
		particleVertices[index] = randomColorR;
		index++;
		particleVertices[index] = randomColorG;
		index++;
		particleVertices[index] = randomColorB;
		index++;
		particleVertices[index] = randomColorA;
		index++;

		// v4
		particleVertices[index] = -particleSize / 2.f + randomValueX;
		index++;
		particleVertices[index] = -particleSize / 2.f + randomValueY;
		index++;
		particleVertices[index] = 0.f;
		index++;
		particleVertices[index] = randomValueVX;
		index++;
		particleVertices[index] = randomValueVY;
		index++;
		particleVertices[index] = randomValueVZ;
		index++;
		particleVertices[index] = randomStartTime;
		index++;
		particleVertices[index] = randomLifeTime;
		index++;
		particleVertices[index] = randomPeriod;
		index++;
		particleVertices[index] = randomAmp;
		index++;
		particleVertices[index] = randomValue;
		index++;
		particleVertices[index] = randomColorR;
		index++;
		particleVertices[index] = randomColorG;
		index++;
		particleVertices[index] = randomColorB;
		index++;
		particleVertices[index] = randomColorA;
		index++;

		// v5
		particleVertices[index] = particleSize / 2.f + randomValueX;
		index++;
		particleVertices[index] = particleSize / 2.f + randomValueY;
		index++;
		particleVertices[index] = 0.f;
		index++;
		particleVertices[index] = randomValueVX;
		index++;
		particleVertices[index] = randomValueVY;
		index++;
		particleVertices[index] = randomValueVZ;
		index++;
		particleVertices[index] = randomStartTime;
		index++;
		particleVertices[index] = randomLifeTime;
		index++;
		particleVertices[index] = randomPeriod;
		index++;
		particleVertices[index] = randomAmp;
		index++;
		particleVertices[index] = randomValue;
		index++;
		particleVertices[index] = randomColorR;
		index++;
		particleVertices[index] = randomColorG;
		index++;
		particleVertices[index] = randomColorB;
		index++;
		particleVertices[index] = randomColorA;
		index++;

		// v6
		particleVertices[index] = -particleSize / 2.f + randomValueX;
		index++;
		particleVertices[index] = particleSize / 2.f + randomValueY;
		index++;
		particleVertices[index] = 0.f;
		index++;
		particleVertices[index] = randomValueVX;
		index++;
		particleVertices[index] = randomValueVY;
		index++;
		particleVertices[index] = randomValueVZ;
		index++;
		particleVertices[index] = randomStartTime;
		index++;
		particleVertices[index] = randomLifeTime;
		index++;
		particleVertices[index] = randomPeriod;
		index++;
		particleVertices[index] = randomAmp;
		index++;
		particleVertices[index] = randomValue;
		index++;
		particleVertices[index] = randomColorR;
		index++;
		particleVertices[index] = randomColorG;
		index++;
		particleVertices[index] = randomColorB;
		index++;
		particleVertices[index] = randomColorA;
		index++;
	}
	glGenBuffers(1, &m_VBOManyParticle);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBOManyParticle);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * floatCount, particleVertices, GL_STATIC_DRAW);
	m_VBOManyParticleCount = vertexCount;
}
void Renderer::AddShader(GLuint ShaderProgram, const char* pShaderText, GLenum ShaderType)
{
	//쉐이더 오브젝트 생성
	GLuint ShaderObj = glCreateShader(ShaderType);

	if (ShaderObj == 0) {
		fprintf(stderr, "Error creating shader type %d\n", ShaderType);
	}

	const GLchar* p[1];
	p[0] = pShaderText;
	GLint Lengths[1];
	Lengths[0] = (GLint)strlen(pShaderText);
	//쉐이더 코드를 쉐이더 오브젝트에 할당
	glShaderSource(ShaderObj, 1, p, Lengths);

	//할당된 쉐이더 코드를 컴파일
	glCompileShader(ShaderObj);

	GLint success;
	// ShaderObj 가 성공적으로 컴파일 되었는지 확인
	glGetShaderiv(ShaderObj, GL_COMPILE_STATUS, &success);
	if (!success) {
		GLchar InfoLog[1024];

		//OpenGL 의 shader log 데이터를 가져옴
		glGetShaderInfoLog(ShaderObj, 1024, NULL, InfoLog);
		fprintf(stderr, "Error compiling shader type %d: '%s'\n", ShaderType, InfoLog);
		printf("%s \n", pShaderText);
	}

	// ShaderProgram 에 attach!!
	glAttachShader(ShaderProgram, ShaderObj);
}

bool Renderer::ReadFile(char* filename, std::string *target)
{
	std::ifstream file(filename);
	if (file.fail())
	{
		std::cout << filename << " file loading failed.. \n";
		file.close();
		return false;
	}
	std::string line;
	while (getline(file, line)) {
		target->append(line.c_str());
		target->append("\n");
	}
	return true;
}

GLuint Renderer::CompileShaders(char* filenameVS, char* filenameFS)
{
	GLuint ShaderProgram = glCreateProgram(); //빈 쉐이더 프로그램 생성

	if (ShaderProgram == 0) { //쉐이더 프로그램이 만들어졌는지 확인
		fprintf(stderr, "Error creating shader program\n");
	}

	std::string vs, fs;

	//shader.vs 가 vs 안으로 로딩됨
	if (!ReadFile(filenameVS, &vs)) {
		printf("Error compiling vertex shader\n");
		return -1;
	};

	//shader.fs 가 fs 안으로 로딩됨
	if (!ReadFile(filenameFS, &fs)) {
		printf("Error compiling fragment shader\n");
		return -1;
	};

	// ShaderProgram 에 vs.c_str() 버텍스 쉐이더를 컴파일한 결과를 attach함
	AddShader(ShaderProgram, vs.c_str(), GL_VERTEX_SHADER);

	// ShaderProgram 에 fs.c_str() 프레그먼트 쉐이더를 컴파일한 결과를 attach함
	AddShader(ShaderProgram, fs.c_str(), GL_FRAGMENT_SHADER);

	GLint Success = 0;
	GLchar ErrorLog[1024] = { 0 };

	//Attach 완료된 shaderProgram 을 링킹함
	glLinkProgram(ShaderProgram);

	//링크가 성공했는지 확인
	glGetProgramiv(ShaderProgram, GL_LINK_STATUS, &Success);

	if (Success == 0) {
		// shader program 로그를 받아옴
		glGetProgramInfoLog(ShaderProgram, sizeof(ErrorLog), NULL, ErrorLog);
		std::cout << filenameVS << ", " << filenameFS << " Error linking shader program\n" << ErrorLog;
		return -1;
	}

	glValidateProgram(ShaderProgram);
	glGetProgramiv(ShaderProgram, GL_VALIDATE_STATUS, &Success);
	if (!Success) {
		glGetProgramInfoLog(ShaderProgram, sizeof(ErrorLog), NULL, ErrorLog);
		std::cout << filenameVS << ", " << filenameFS << " Error validating shader program\n" << ErrorLog;
		return -1;
	}

	glUseProgram(ShaderProgram);
	std::cout << filenameVS << ", " << filenameFS << " Shader compiling is done.\n";

	return ShaderProgram;
}
unsigned char * Renderer::loadBMPRaw(const char * imagepath, unsigned int& outWidth, unsigned int& outHeight)
{
	std::cout << "Loading bmp file " << imagepath << " ... " << std::endl;
	outWidth = -1;
	outHeight = -1;
	// Data read from the header of the BMP file
	unsigned char header[54];
	unsigned int dataPos;
	unsigned int imageSize;
	// Actual RGB data
	unsigned char * data;

	// Open the file
	FILE * file = NULL;
	fopen_s(&file, imagepath, "rb");
	if (!file)
	{
		std::cout << "Image could not be opened, " << imagepath << " is missing. " << std::endl;
		return NULL;
	}

	if (fread(header, 1, 54, file) != 54)
	{
		std::cout << imagepath << " is not a correct BMP file. " << std::endl;
		return NULL;
	}

	if (header[0] != 'B' || header[1] != 'M')
	{
		std::cout << imagepath << " is not a correct BMP file. " << std::endl;
		return NULL;
	}

	if (*(int*)&(header[0x1E]) != 0)
	{
		std::cout << imagepath << " is not a correct BMP file. " << std::endl;
		return NULL;
	}

	if (*(int*)&(header[0x1C]) != 24)
	{
		std::cout << imagepath << " is not a correct BMP file. " << std::endl;
		return NULL;
	}

	dataPos = *(int*)&(header[0x0A]);
	imageSize = *(int*)&(header[0x22]);
	outWidth = *(int*)&(header[0x12]);
	outHeight = *(int*)&(header[0x16]);

	if (imageSize == 0)
		imageSize = outWidth * outHeight * 3;

	if (dataPos == 0)
		dataPos = 54;

	data = new unsigned char[imageSize];

	fread(data, 1, imageSize, file);

	fclose(file);

	std::cout << imagepath << " is succesfully loaded. " << std::endl;

	return data;
}

GLuint Renderer::CreatePngTexture(char * filePath)
{
	//Load Pngs: Load file and decode image.
	std::vector<unsigned char> image;
	unsigned width, height;
	unsigned error = lodepng::decode(image, width, height, filePath);
	if (error != 0)
	{
		lodepng_error_text(error);
		assert(error == 0);
		return -1;
	}

	GLuint temp;
	glGenTextures(1, &temp);

	glBindTexture(GL_TEXTURE_2D, temp);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, &image[0]);

	return temp;
}

GLuint Renderer::CreateBmpTexture(char * filePath)
{
	//Load Bmp: Load file and decode image.
	unsigned int width, height;
	unsigned char * bmp
		= loadBMPRaw(filePath, width, height);

	if (bmp == NULL)
	{
		std::cout << "Error while loading bmp file : " << filePath << std::endl;
		assert(bmp != NULL);
		return -1;
	}

	GLuint temp;
	glGenTextures(1, &temp);

	glBindTexture(GL_TEXTURE_2D, temp);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, bmp);

	return temp;
}

void Renderer::Test()
{

	//-------------------------------그리기위한 준비 prepare step.--------
	glUseProgram(m_SolidRectShader);

	int attribPosition = glGetAttribLocation(m_SolidRectShader, "a_Position"); //next time
	glEnableVertexAttribArray(attribPosition); // next time
	glBindBuffer(GL_ARRAY_BUFFER, m_VBOTest); // m_VBOTest --> bind --> GL_ARRAY_BUFFER
	glVertexAttribPointer(attribPosition, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 3, 0);
	//인자: size(x,y,z => 3개 이므로 3), type() , stride(정점간의 단위 ?, 주소값을 읽어내는 Gap)
	//		ex) 처음0번지-> 다음 5번지 , 현재는 0번지-> 다음은 3번지 이렇게될거같음. 현재우리는 정점1개를 float * 3 크기만큼 정했음.
	// 마지막인자 pointer는 시작 번지를 알려줌 0번지메모리부터 읽어라!(처음부터 읽어라.) 
	//--------------------------------------------------------------------

	glDrawArrays(GL_LINE_LOOP, 0, 3); // 인자: 프리미티브, 어디서부터시작할건지, 

	glDisableVertexAttribArray(attribPosition); 
}

void Renderer::DrawSign()
{
	glUseProgram(m_SolidRectShader);

	int attribPosition = glGetAttribLocation(m_SolidRectShader, "a_Position"); //next time
	glEnableVertexAttribArray(attribPosition); // next time
	glBindBuffer(GL_ARRAY_BUFFER, m_VBOTest); // m_VBOTest --> bind --> GL_ARRAY_BUFFER
	glVertexAttribPointer(attribPosition, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 3, 0);

	glDrawArrays(GL_LINE_STRIP, 0, 3); // 인자: 프리미티브, 어디서부터시작할건지, 
	glDrawArrays(GL_LINE_STRIP, 3, 5);
	glDrawArrays(GL_LINE_STRIP, 8, 4);
	glDrawArrays(GL_LINES, 12, 2);
	//glDrawArrays(GL_LINE_STRIP, 5, 4);
	

	glDisableVertexAttribArray(attribPosition);
}


/* 
[시험]
극좌표계를 이용해 조그만 사각형을 원 삥삥돌게 하기
*/
void Renderer::Lecture3(float elapsedTime)   //파티클 관련된 study를 위한것.
{
	GLuint shader = m_SolidRectShader;  //프로그램이름을 참조하기위함.
	glUseProgram(shader);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glBindBuffer(GL_ARRAY_BUFFER, m_VBOManyParticle); // 6개의 버텍스
	
	GLuint posID = glGetAttribLocation(shader, "a_Position"); //get을 해야만 정확한 id를 받아올수있다.
	glEnableVertexAttribArray(posID);  //0번을 쓰겠다. 레이아웃과 싱크가 맞아야함. 
	glVertexAttribPointer(posID, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 15, (GLvoid*)0); //첫번쨰인자: 레이아웃인덱스, 
	
	GLuint VelID = glGetAttribLocation(shader, "a_Vel");
	glEnableVertexAttribArray(VelID);
	glVertexAttribPointer(VelID, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 15, (GLvoid*)(sizeof(float) * 3)); //첫번쨰인자: 레이아웃인덱스, 
	
	//이와같이 sizeof * 7 이런것처럼 이런 인자 계산하여 하는것 시험에낼것임.
	GLuint StartTimeID = glGetAttribLocation(shader, "a_StartTime");
	glEnableVertexAttribArray(StartTimeID);
	glVertexAttribPointer(StartTimeID, 1, GL_FLOAT, GL_FALSE, sizeof(float) * 15, (GLvoid*)(sizeof(float) * 6)); //첫번쨰인자: 레이아웃인덱스,

	GLuint LifeTimeID = glGetAttribLocation(shader, "a_LifeTime");
	glEnableVertexAttribArray(LifeTimeID);
	glVertexAttribPointer(LifeTimeID, 1, GL_FLOAT, GL_FALSE, sizeof(float) * 15, (GLvoid*)(sizeof(float) * 7)); //첫번쨰인자: 레이아웃인덱스, 

	GLuint PeriodID = glGetAttribLocation(shader, "a_Period");
	glEnableVertexAttribArray(PeriodID);
	glVertexAttribPointer(PeriodID, 1, GL_FLOAT, GL_FALSE, sizeof(float) * 15, (GLvoid*)(sizeof(float) * 8)); //첫번쨰인자: 레이아웃인덱스,

	GLuint AmpID = glGetAttribLocation(shader, "a_Amp");
	glEnableVertexAttribArray(AmpID);
	glVertexAttribPointer(AmpID, 1, GL_FLOAT, GL_FALSE, sizeof(float) * 15, (GLvoid*)(sizeof(float) * 9)); //첫번쨰인자: 레이아웃인덱스, 

	GLuint ValueID = glGetAttribLocation(shader, "a_Value");
	glEnableVertexAttribArray(ValueID);
	glVertexAttribPointer(ValueID, 1, GL_FLOAT, GL_FALSE, sizeof(float) * 15, (GLvoid*)(sizeof(float) * 10)); //첫번쨰인자: 레이아웃인덱스, 

	GLuint timeID = glGetUniformLocation(shader, "u_Time");
	glUniform1f(timeID, m_TotalTimeLecture3);

	GLuint colorID = glGetAttribLocation(shader, "a_Color");
	glEnableVertexAttribArray(colorID);
	glVertexAttribPointer(colorID, 4, GL_FLOAT, GL_FALSE, sizeof(float) * 15, (GLvoid*)(sizeof(float) * 11)); //첫번쨰인자: 레이아웃인덱스, 

	

	

	glDrawArrays(GL_TRIANGLES, 0, m_VBOManyParticleCount);
	m_TotalTimeLecture3 += elapsedTime;   //애니메이션을 위해 토탈시간에 더해줌
	glDisable(GL_BLEND);
} // 1. VBO에 추가 2. SHADER추가 3. ATTRIPOINT Update 


// 1. VBO-> 정보추가. Particle 당 life Time -> VBO에 추가
// 2. Attrib Array => Stride,    ... 내용 수정
// 3. VS에서 life time -> Attrib 선언 -> life time