#version 450

 in vec3 a_Position;   //vs input 선언, float 3개, layout이 어떤것인지모른다. 그래서 컴파일시에 자동으로 id를 만들어준다
 in vec3 a_Vel; // vs input 선언, float 3개, 속도 형태로 사용할것임.
 in float a_StartTime; // vs input, float 1개, 시작 시간을 위한것.
 in vec4 a_Color;
 in float a_LifeTime; // vs input, float 1개, 생명(초/시간)
 in float a_Period;
 in float a_Amp;
 in float a_Value;
 out vec4 v_Color; // vertex shader output
 
uniform float u_Time;
vec3 c_Gravity = vec3(0, -0.25, 0);
bool c_bLoop = true;
float c_PI = 3.141592;

void main()
{
	
	
	 // -1 ~ 1 사이의값 출력(x, y), z값은 depth
	
	//시험에 낼것임 . 주기를 다르게 하여

	float newTime = u_Time - a_StartTime; // 생성 이후 지난시간
	vec2 initParametricPos = vec2(0.7*sin(a_Value * 2.0 * c_PI ), 0.7*cos(a_Value * 2.0 * c_PI ));
	vec4 newPos = vec4(a_Position.xy + initParametricPos, 0, 1); //초기위치

	float period = a_Period; // 주기
	float amp = a_Amp; // 폭
	float alpha = 0.0f;

	if(newTime > 0)
	{
	
		float TempTime = newTime;
		if(c_bLoop)
		{
			alpha = fract(TempTime / a_LifeTime);
			TempTime = alpha * a_LifeTime; //fract는 정수부분만 알기위함.
												//intrinsic functions -> 자주 사용되는 수학식과 같은것을 제공한다.
												//ex)  floor -> 내림 , round -> 반올림
		}
		
		//주기가 점점 작아지도록 ㅋ하기 -> 시험

		newPos.xyz = newPos.xyz + a_Vel.xyz * TempTime +0.5 * c_Gravity * TempTime * TempTime;

		vec2 rotVel = vec2(-(a_Vel.y + c_Gravity.y * TempTime), (a_Vel.x + c_Gravity.y * TempTime));
		rotVel = normalize(rotVel);
		newPos.xy = newPos.xy + TempTime * rotVel* amp * sin(2.0f * c_PI * TempTime * period );


		//newPos.y = newPos.y + TempTime * rotVel.y* amp * sin(2.0f * c_PI * TempTime * period );
		//newPos.x = newPos.x + TempTime * rotVel.x* amp * sin(2.0f * c_PI * TempTime * period );
	}
	else
	{
		newPos = vec4(-100000,100000,0,1);
	}
	v_Color = vec4(a_Color.rgb, (1.0 - alpha ) );
	gl_Position = newPos;
}




/*float newTime = u_Time - a_StartTime; // 생성 이후 지난시간
	vec4 newPos = vec4(a_Position, 1);	
	if(newTime > 0)
	{
		float TempTime = newTime;
		if(c_bLoop)
		{
			TempTime = fract(TempTime / a_LifeTime) * a_LifeTime; //fract는 정수부분만 알기위함.
												//intrinsic functions -> 자주 사용되는 수학식과 같은것을 제공한다.
												//ex)  floor -> 내림 , round -> 반올림
		}
		//p = p0 + TempTime * vel + 0.5*c_gravity.xyz * TempTime*TempTime 
		newPos.xyz = a_Position.xyz + a_Vel.xyz * TempTime + 0.5 * c_Gravity * TempTime * TempTime;
	}
	else
	{

	}*/